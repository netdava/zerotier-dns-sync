(ns com.netdava.zerotier.core
  (:require [com.netdava.zerotier.dns-sync :as ds]))

(defn -main 
  [{zt-network-name :zerotier-network-name cf-zone-name :cloudflare-zone-name} ds/api-token ds/cloudflare-api-token]
  (let [machines (ds/zt-machines-to-update-or-add zt-network-name cf-zone-name ds/api-token ds/cloudflare-api-token)]
  (do (ds/update-cloudflare-records! machines ds/clouflare-api-token)
      (ds/create-cloudflare-records! machines ds/cloudflare-api-token))))