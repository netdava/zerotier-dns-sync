(ns com.netdava.zerotier.dns-sync
  (:require [clj-http.lite.client :as client]
            [clojure.data.json :as json]
            [clojure.set :as set]
            [clojure.tools.cli :refer [parse-opts]]
            [taoensso.timbre :as log]))

#_(let [c (slurp "logging.edn")
      cc (read-string c)]
  (println cc)
  (log/merge-config! cc))

(def api-token (System/getenv "ZEROTIER_SYNC_API_TOKEN"))
(def cloudflare-api-token (System/getenv "CLOUDFLARE_SYNC_API_TOKEN"))

(defn only [x] {:pre [(nil? (next x))]} (first x))

(defn get-network!
  "Fetch network data from zerotier."
  ;; TODO: error handling - doar pentru cod 200 avem body json ok
  [token & opts]
  (try
    (let [api-endpoint (or (:api-endpoint opts) "https://my.zerotier.com/api/v1/network")
          response (client/get api-endpoint
                               {:headers {"Authorization" (str "bearer " token)}})
          body (:body response)]
      (log/info "Se iau datele despre retea din zerotier...")
      (json/read-str body :key-fn keyword))
    (catch Exception e (log/error "Datele despre retea din zerotier nu au putut fi luate" (.getMessage e)))))

(defn get-network-members!
  "Fetch network-members data from zerotier"
  ;; TODO: error handling
  [network-id token & opts]
  (let [url "https://my.zerotier.com/api/v1/network"
        api-endpoint (or (:api-endpoint opts) url)
        url (str api-endpoint "/" network-id "/member")
        response (client/get url
                             {:headers {"Authorization" (str "bearer " token)}})
        body (get response :body)
        json (json/read-str body :key-fn keyword)]
    json))

(defn zerotier-authorized-members
  "Filter authorized network-members data from zerotier"
  [members]
  (filter #(true? (:authorized (:config %))) members))

(defn get-cloudflare-zones!
  "Fetch zones data from cloudflare."
  [token & opts]
  (let [api-endpoint (or (:api-endpoint opts) "https://api.cloudflare.com/client/v4/zones/")
        response (client/get api-endpoint
                             {:headers {"Authorization" (str "bearer " token)}})
        body (:body response)]
    (json/read-str body :key-fn keyword)))

(defn cloudflare-zone
  "Find zone data from zones list"
  [zones zone-name]
  (let [l (:result zones)
        zone (filter #(= zone-name (:name %)) l)]
    (only zone)))

(defn get-zone-dns-records!
  "Fetch cloudflare zone dns records data."
  [zone token & opts]
  (let [url "https://api.cloudflare.com/client/v4/zones/"
        api-endpoint (or (:api-endpoint opts) url)
        id (:id zone)
        call-args "/dns_records?name=contains%3Az1&per_page=50&match=any"
        url (str api-endpoint id call-args)
        response (client/get url
                             {:headers {"Authorization" (str "Bearer " token)
                                        "Content-Type" "application/json"}})
        body (:body response)]
    (json/read-str body :key-fn keyword)))

(defn zt-members-new-structure
  [members]
  (map (fn [m] (let [name (:name m)
                     ip (:ipAssignments (:config m))
                     name (str name ".z1.netdava.com")]
                 {:name name :IP ip})) members))

(defn records-new-structure
  [records]
  (map (fn [r] (let [name (:name r)
                     ip (:content r)]
                 {:name name :IP [ip]})) records))

(defn find-member-to-update [records m]
  (some #(and (not= (:IP %) (:IP m))
              (= (:name %) (:name m))) records))

(defn machines-to-update
  "Find zerotier network members that have different IP address in cloudflare."
  [records network-members]
  (let [diff-members (filter (partial find-member-to-update records) network-members)]
    (into [] diff-members)))

(defn machines-to-create-as-new-records
  "Find zerotier network members that need to be created as dns records in cloudflare."
  [members records machines]
  (let [m-not-created (set/difference (set/difference (set members) (set records))
                                      (set machines))]
    (into [] m-not-created)))

(defn extract-machine-full-info-for-update
  "Rebuild map structure for machines that need to be updated in cloudflare."
  [machines records]
  (let [d (map (fn [machine] (let [mach-name (:name machine)
                                   IP (:IP machine)]
                               (map (fn [record] (when (= mach-name (:name record)) {:type (:type record) :name mach-name :IP IP :ttl (:ttl record) :zone-id (:zone_id record) :id (:id record)})) records))) machines)
        d-with-no-nils (filter #(not= nil %) (flatten d))]
    (into [] d-with-no-nils)))

(defn extract-machine-full-info-for-create
  "Rebuild map structure for machines that need to be created in cloudflare."
  [machines members z-id]
  (let [match-name-ip (fn match-name-ip [machine]
                        (let [mach-name (:name machine)
                              IP (:IP machine)]
                          (map (fn [member]
                                 (when (= mach-name (str (:name member) ".z1.netdava.com"))
                                   {:type "A" :name mach-name :IP IP :ttl 60 :zone_id z-id})) members)))
        d (map match-name-ip machines)
        d-with-no-nils (filter #(not= nil %) (flatten d))]
    (into [] d-with-no-nils)))

(defn zt-machines-to-update-or-add
  "Extract two sets of data about the machines that need to be updated
  and machines that need to be added in cloudflare."
  [zerotier-network-name cloudflare-zone-name zt-token cf-token]
  (let [zones (get-cloudflare-zones! cf-token)
        zone (cloudflare-zone zones cloudflare-zone-name)
        z-id (:id zone)
        records (:result (get-zone-dns-records! zone cf-token))
        zt-data (get-network! zt-token)
        zt-network-data (filter #(= zerotier-network-name (:name (:config %))) zt-data)
        zt-network-id (:id (into {} zt-network-data))
        zt-authorised-members (zerotier-authorized-members (get-network-members! zt-network-id zt-token))
        mbrs (into [] zt-authorised-members)
        zt-members_new (zt-members-new-structure zt-authorised-members)
        records_new (records-new-structure records)
        m-to-update (machines-to-update records_new zt-members_new)
        m-to-add (machines-to-create-as-new-records zt-members_new records_new m-to-update)]
    {:to-update (extract-machine-full-info-for-update m-to-update records)
     :to-add (extract-machine-full-info-for-create m-to-add zt-authorised-members z-id)
     :m mbrs}))

(defn update-cloudflare-records!
  [machines token & opts]
  (let [api-endpoint (or (:api-endpoint opts) "https://api.cloudflare.com/client/v4/zones/")
        headers {"Authorization" (str "bearer " token)
                 "Content-Type" "application/json"
                 "Accept" "application/json"}]
    (when (empty? machines) (log/info "Nu am gasit membri din retea pentru care sa se faca update in cloudflare"))
    (map (fn [m] (let [{:keys [zone-id id name type IP ttl]} m
                       ip (first IP)
                       url (str api-endpoint zone-id "/dns_records/" id)
                       update-map {"type" type
                                   "name" name
                                   "content" ip
                                   "ttl" ttl
                                   "proxied" false}]
                   (log/info "se updateaza dns record pentru masina " name " cu IP " ip)
                   (client/put url {:headers headers
                                    :body (json/write-str update-map)}))) machines)))

(defn create-cloudflare-records!
  [machines token & opts]
  (let [api-endpoint (or (:api-endpoint opts) "https://api.cloudflare.com/client/v4/zones/")
        headers {"Authorization" (str "bearer " token)
                 "Content-Type" "application/json"
                 "Accept" "application/json"}]
    (when (empty? machines) (log/info "Nu am gasit membri din retea pentru care sa se creeze dns-records in cloudflare"))
    (map (fn [m] (let [{:keys [zone_id type name IP ttl]} m
                       ip (first IP)
                       url (str api-endpoint zone_id "/dns_records")
                       create-map {"type" type
                                   "name" name
                                   "content" ip
                                   "ttl" ttl
                                   "proxied" false}]
                   (log/info "se creeaza dns record pentru masina " name " cu IP " ip)
                   (client/post url
                                {:headers headers
                                 :body (json/write-str create-map)}))) machines)))

(defn update-records!
  [zt-network-name cf-zone-name api-token cloudflare-api-token]
  (let [machines (zt-machines-to-update-or-add zt-network-name cf-zone-name api-token cloudflare-api-token)
        m-to-update (:to-update machines)
        mbrs (:m machines)
        membri (into [] (map (fn [m] (:name m)) mbrs))]
    (log/info "Am gasit urmatorii membri ai retelei: " membri)
    (update-cloudflare-records! m-to-update cloudflare-api-token)))

(defn create-records!
  [zt-network-name cf-zone-name api-token cloudflare-api-token]
  (let [machines (zt-machines-to-update-or-add zt-network-name cf-zone-name api-token cloudflare-api-token)
        m-to-add (:to-add machines)]
    (create-cloudflare-records! m-to-add cloudflare-api-token)))

(def cli-options
  [["-n" "--zt-network-name ZERO_TIER_NETWORK_NAME" "Zero tier network name"
    :default "z1"]
   ["-z" "--cloudflare-zone-name CLODFLARE_ZONE_NAME" "Cloudflare zone name"
    :default "netdava.com"]
   ["-r" "--zerotier-api-token ZEROTIER_API_TOKEN" "Zerotier API Token"
    :default api-token]
   ["-e" "--cloudflare-api-token CLOUDFLARE_API_TOKEN" "Cloudflare API Token"
    :default cloudflare-api-token]
   ["-h" "--help"]])

(defn custom-parse-opts
  [cli-args cli-opts]
  (let [parsed-opts (parse-opts cli-args cli-opts)
        {:keys [summary errors]
         {:keys [help]} :options} parsed-opts]
                            ;;  (println parsed-opts)
    (when errors
      (println "Allowed options are:\n" summary "\n"
               "Error parsing args:\n" errors "\n"
               "Parsed options are:\n" parsed-opts "\n")
      (System/exit 1))
    (when help
      (println "Allowed options are:\n" summary "\n")
      (System/exit 0))
    parsed-opts))

(defn -main
  [& args]
  (let [ar (custom-parse-opts args cli-options)
        opt (:options ar)
        ztn-name (:zt-network-name opt)
        cfz-name (:cloudflare-zone-name opt)
        zt-api (:zerotier-api-token opt)
        cf-api (:cloudflare-api-token opt)]
    (log/info "Urmeaza update pentru membrii retelei " ztn-name ".netdava.com")
    (dorun (update-records! ztn-name cfz-name zt-api cf-api))
    (log/info "Urmeaza creearea de dns-records pentru zona " ztn-name ".netdava.com")
    (dorun (create-records! ztn-name cfz-name zt-api cf-api))))

(comment
  (-main)
  (let [c (slurp "logging.edn")
        cc (read-string c)]
    (println cc)
    (log/merge-config! cc))

  (get-network! api-token)
  (let [vecA [{:name "Home Desktop" :IP "192.168.0.1" :id "hhhhhhhhh"} {:name "Work Station" :IP "192.0.0.1" :id "ggg"}]
        vecB [{:name "Home Desktop" :IP "178.138.35.01"} {:name "Work Station" :IP "178.138.35.22"}]]
    (map (fn [i] (let [nameA (:name i)]
                   (map (fn [ii] (when (= nameA (:name ii)) {:name (:name ii) :IP (:IP ii)})) vecB))) vecA)))