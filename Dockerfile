FROM clojure:openjdk-17-tools-deps-buster

WORKDIR /app

COPY src /app/src
COPY *.edn /app/

RUN clj -P -M:run-m

CMD ["clj", "-J-Dclojure.main.report=stderr", "-M:run-m"]
